$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scroll-top').fadeIn();
        } else {
            $('.scroll-top').fadeOut();
        }
    });

    $('.scroll-top').on('click',function () {
        var body = $("html, body");
        body.stop().animate({scrollTop:0}, 900, 'swing', function() {
            console.log('roll..');
        });
    });


    $('.call').on('mouseenter',function(){
        $('.call').text('087-6165971');
    });
});