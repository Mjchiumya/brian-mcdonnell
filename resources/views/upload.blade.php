@extends('layouts.app')

@section('upload')

    <div class="container contact">
        <div class="row">

            <div class="col-md-3">
                <div class="contact-info">
                    <img src="https://image.ibb.co/kUASdV/contact-image.png" alt="image"/>
                    <h2>image uploads</h2>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>


            <div class="col-md-9">
                <form class="contact-form" method="post" action="{{url('img-job')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Title:</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" id="image" placeholder="upload" name="image" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" id="id" name="id" value="{{Request::segment(2)}}">
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection