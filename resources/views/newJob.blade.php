@extends('layouts.app')

@section('newjob')

    <div class="container contact">
        <div class="row">
            <div class="col-md-3">
                <div class="contacts">
                    <h4>Brian McDonnell
                        Add new Job form</h4>

                    <h4>fill in all fields !</h4>
                </div>
            </div>
            <div class="col-md-9">
                <form class="contact-form" method="post" action="{{url('put-job')}}">
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Title:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" placeholder="title.." name="name" required>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-sm-2" for="desc">Comment:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="5" id="desc" name="description" required></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
