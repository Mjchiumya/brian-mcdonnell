@extends('pages.template_page')

@section('content')
    <br><br><br><br>
    <div class="container mt-5">

        <div class="row ">
            <?php
              foreach ($jobs as $job){
            echo '
            <div class="col-md-3">
                <div class="card mb-2">
                   <div class="text-center">
                     <img class="img-fluid" src="storage/uploads/'.$job->image.'" class="rounded" alt="Cinque Terre" width="304" height="236" alt="brianMcDonnell">
                     </div>
                     <div class="card-body">
                       <h5 class=" card-title">'.$job->name.'</h5>
                          <p class="card-text">'.$job->description.'</p>
                        <p class="card-text">
                          <small class="text-time"><em>'.$job->created_at.'</em></small>
                       </p>
                    </div>
                </div>
           </div>
<br><br>
';}?>
        </div>
    </div>
 @endsection
