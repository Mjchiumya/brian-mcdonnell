@extends('pages.template_page')

@section('content')
    <br><br><br>
<!--site map-->
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/home">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Services</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="jumbotron">
        <div class="row w-100">
            <div class="col-md-3">
                <div class="card border-secondary mx-sm-1 p-3">
                    <div class="card border-light shadow text-info p-3 my-card" ><span class="fa fa-check" aria-hidden="true"></span></div>
                    <div class="text-secondary text-center mt-3"><h4>Vehicle Recovery</h4></div>
                    <div class="text-info text-center mt-2"><h1></h1></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card border-secondary mx-sm-1 p-3">
                    <div class="card border-light shadow text-info p-3 my-card"><span class="fa fa-check" aria-hidden="true"></span></div>
                    <div class="text-secondary text-center mt-3"><h4>Concrete Structures transportation</h4></div>
                    <div class="text-success text-center mt-2"><h1></h1></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card border-secondary mx-sm-1 p-3">
                    <div class="card border-light shadow text-info p-3 my-card" ><span class="fa fa-check" aria-hidden="true"></span></div>
                    <div class="text-secondary text-center mt-3"><h4>Heavy Machinery Transportation</h4></div>
                    <div class="text-danger text-center mt-2"><h1></h1></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card border-secondary mx-sm-1 p-3">
                    <div class="card border-light shadow text-info p-3 my-card" ><span class="fa fa-check" aria-hidden="true"></span></div>
                    <div class="text-secondary text-center mt-3"><h4>Heavy Equipment Transportation</h4></div>
                    <div class="text-warning text-center mt-2"><h1></h1></div>
                </div>
            </div>
        </div>
    </div>
@endsection