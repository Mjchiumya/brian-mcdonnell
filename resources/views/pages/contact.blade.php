@extends('pages.template_page')

@section('content')
     <br><br><br>
    <!--<div class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Brian McDonnell</h1>
            <p class="lead text-muted mb-0">Recovery and Transportation Services</p>
        </div>
    </div>-->
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <nav aria-label="breadcrumb bg-white">
                    <ol class="breadcrumb ">
                        <li class="breadcrumb-item"><a href="/home">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Contact</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header text-grey"><h4>Contact us with Quick Mail on form below.</h4>
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <form method="post" action={{url('email')}}>
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name" aria-describedby="emailHelp" placeholder="Enter name" >
                            </div>

                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter email" >
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea class="form-control" name="message" rows="6" ></textarea>
                            </div>
                            <div class="mx-auto">
                                <button type="submit" class="btn text-right btn-dark">Submit</button></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="card bg-grey mb-3">
                    <div class="card-header bg-grey text-grey text-uppercase"><i class="fa fa-home"></i> Address</div>
                    <div class="card-body">
                         <h3 class="h6 font-italic"><i class="fa fa-home"></i> : Clonygowan Co. Ofally</h3>
                         <h3  class="h6 font-italic"><i class="fa fa-envelope"></i> : customer-service@brianmcdonnell.ie</h3>
                         <h3  class="h6 font-italic"><i class="fa fa-phone"></i> : +353 876 165971</h3>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
