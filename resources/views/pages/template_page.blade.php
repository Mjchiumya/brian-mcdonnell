<!DOCTYPE html>
<html lang="en">
<head>
    <title>Brian McDonnell Recovery</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="descprition" content="Brian McDonnell Recovery services offer a wide range of transportation services including transporting scrappage vehicle,heavy equipment,machinery,fortlifts,etc we also offer vehicle recovery services"
    <link rel="shortcut icon" type="image/png" href="{{URL::asset('images/icon.png')}}"/>
    <link rel="stylesheet" href="{{URL::asset('css/main.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/carousel.css')}}">
</head>

<body>
<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand  p-0 m-0" href="{{ url('home') }}">
        <img src="{{URL::asset('images/logo.png')}}" class="img-fluid w-25" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="{{ url('home') }}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('services') }}">Services</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('our-work') }}">Our Work</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    More
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ url('contact') }}">Contact us</a>
                    <a class="dropdown-item" href="{{url('faq') }}">FAQ</a>
                </div>
            </li>
        </ul>
    </div>
    <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
        
        <li class="nav-item">
            <a class="nav-link font-italic" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">087-6165971</a>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    @yield('content')

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script src="{{ URL::asset('js/main.js')}}"></script>
</body>
</html>
