<!DOCTYPE html>
<html lang="en">
<head>
  <title>Brian McDonnell Recovery</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" type="image/png" href="{{URL::asset('images/icon.png')}}"/>
  <link rel="stylesheet" href="{{URL::asset('css/main.css')}}">
  <link rel="stylesheet" href="{{URL::asset('css/carousel.css')}}">
    <link rel="icon" href="{{URL::asset('images/logo.css')}}">
</head>

<body>
<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="{{ url('home') }}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('services') }}">Services</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('our-work') }}">Our Work</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    More
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ url('contact') }}">Contact us</a>
                    <a class="dropdown-item" href="{{ url('faq') }}">FAQ</a>
                </div>
            </li>
        </ul>
    </div>
    <ul class="nav nav-tabs nav-justified hidden-xs" id="myTab" role="tablist">

        <li class="nav-item">
            <a class="nav-link btn-outline-dark" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">087-6165971</a>
        </li>
    </ul>
</nav>
<br>

<!--slider-->
<section class="slida my-5">

<div class="container-fluid">
            <!-- Full Page Image Background Carousel Header -->
    <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for Slides -->
                <div class="carousel-inner"  role="listbox" >
                    <div class="carousel-item active">
                        <div class="view">
                            <img src="{{asset('/images/recov.jpg')}}" class="d-block w-100">                             
                        </div>
                        <div class="carousel-caption">                           
                            <img src={{asset('/images/logosize.png')}} width="360" class="mx-auto img-fluid animated fadeInRight">                                                       
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="view">
                            <img src="{{asset('/images/container.jpg')}}" class="d-block w-100">                           
                        </div>
                        <div class="carousel-caption d-md-block">
                            <h2 class="h4 animated fadeInRight font-weight-bold font-italic">Container Transportation</h2>
                            <p class="lead animated fadeInRight font-italic d-none d-sm-block">
                                We offer trustworthy container transportation services
                                </p>
                            <p class="animated fadeInRight">
                                <a href="{{url('our-work')}}" class="mx-auto btn w-25 btn-lg btn-outline-secondary bg-light d-none d-sm-block">Learn More</a>
                            </p>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="view">
                            <img src="{{asset('/images/nice.jpg')}}" class="d-block vw-100">
                            <div class="mask rgba-black-slight"></div>
                        </div>
                        <div class="carousel-caption">
                            <h2 class="h4 animated fadeInRight font-weight-bold font-italic">Heavy Machinery & Equipment</h2>
                            <p class="lead animated fadeInRight font-italic d-none d-sm-block">we do all sorts of heavy transportation give us a call today</p>
                            <p class="animated fadeInRight">
                              <a href="{{url('our-work')}}" class="mx-auto btn w-25 btn-lg btn-outline-secondary bg-light d-none d-sm-block">Learn More</a>
                            </p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img src="{{asset('/images/loading.jpg')}}" class="d-block vw-100">
                            <div class="mask rgba-black-slight"></div>
                        </div>
                        <div class="carousel-caption">
                            <h2 class="h4 animated fadeInRight font-weight-bold font-italic">Heavy Equipment For The Job</h2>
                            <p class="lead animated fadeInRight font-italic d-none d-sm-block">we do all sorts of heavy transportation give us a call today</p>
                            <p class="animated fadeInRight">
                                <a href="{{url('our-work')}}" class="mx-auto btn w-25 btn-lg btn-outline-secondary bg-light d-none d-sm-block">Learn More</a>
                           </p>
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="icon-prev"></span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="icon-next"></span>
                </a>
            </div>
</div>

        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span> </a>
              <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span> </a>

    <!--slide end-->
</section>

<section>
    <div class="jumbotron">
        <h1 class="font-weight-bold font-italic">Who we are</h1>
        <p class="lead">We are transportation enforcer offering trustworthy transportation services
            Specialise in heavy machinery,
            construction equipment,farming equipment,scrappage transportation,vehicle transport and recovery.
            We are dedicated to provide quality and reliable services contact us for more information.
        </p>
        <hr class="my-4">
        <p>If you are browsing this website from your mobile phone you can click on the number below to give us a call.</p>
        <a class="btn btn-outline-dark call btn-lg" href="tel:+355876165971" role="button">
            <span class="btn-label"><i class="fa fa-phone"></i></span>
            Call us now</a>
         </div>
</section>

<section>
    <div class="">
        <div class="container">
            <h2 class="pb-3 pt-2 border-bottom mb-5">what we offer</h2>
            <!--first section-->
            <div class="row align-items-center how-it-works d-flex">
                <div class="col-2 text-center bottom d-inline-flex justify-content-center align-items-center">
                    <div class="circle font-weight-bold">1</div>
                </div>
                <div class="col-6">
                    <h5 class="font-weight-bold font-italic">Steel & Concrete Structures Transportation</h5>
                    <p class="font-italic">Heavy steel structures,industrial steel,heavy load wires.</p>
                </div>
            </div>
            <!--path between 1-2-->
            <div class="row timeline">
                <div class="col-2">
                    <div class="corner top-right"></div>
                </div>
                <div class="col-8">
                    <hr/>
                </div>
                <div class="col-2">
                    <div class="corner left-bottom"></div>
                </div>
            </div>
            <!--second section-->
            <div class="row align-items-center justify-content-end how-it-works d-flex">
                <div class="col-6 text-right">
                    <h5 class="font-weight-bold font-italic">Scrap car transportation</h5>
                    <p class="font-italic">Transport vehicles to scrap</p>
                </div>
                <div class="col-2 text-center full d-inline-flex justify-content-center align-items-center">
                    <div class="circle font-weight-bold">2</div>
                </div>
            </div>
            <!--path between 2-3-->
            <div class="row timeline">
                <div class="col-2">
                    <div class="corner right-bottom"></div>
                </div>
                <div class="col-8">
                    <hr/>
                </div>
                <div class="col-2">
                    <div class="corner top-left"></div>
                </div>
            </div>
            <!--third section-->
            <div class="row align-items-center how-it-works d-flex">
                <div class="col-2 text-center top d-inline-flex justify-content-center align-items-center">
                    <div class="circle font-weight-bold">3</div>
                </div>
                <div class="col-6">
                    <h5 class="font-weight-bold font-italic">Construction & Maintenance Equipment Transportation</h5>
                    <p class="font-italic">Forklifts, Road maintenance equipment,building equipment .</p>
                </div>
            </div>

            <!--path between 1-2-->
            <div class="row timeline">
                <div class="col-2">
                    <div class="corner top-right"></div>
                </div>
                <div class="col-8">
                    <hr/>
                </div>
                <div class="col-2">
                    <div class="corner left-bottom"></div>
                </div>
            </div>
            <!--second section-->
            <div class="row align-items-center justify-content-end how-it-works d-flex">
                <div class="col-6 text-right">
                    <h5 class="font-weight-bold font-italic">Farm Loads Transportation</h5>
                    <p class="font-italic">Heavy farm equipment and tools</p>
                </div>
                <div class="col-2 text-center full d-inline-flex justify-content-center align-items-center">
                    <div class="circle font-weight-bold">4</div>
                </div>
            </div>
        </div>
    </div>
</section>
<br>
<br>
<section>
    <!--Jumbotron-->
    <div class="jumbotron text-center mdb-color lighten-2 white-text z-depth-2">
                     <center> <img src="{{URL::asset('images/logosize.png')}}" class="img-fluid"/></center>
                     <br>
        <!--
        <h1 class="card-title h2-responsive mt-2 font-bold"><strong>Brian McDonnell</strong></h1>
       
        <p class="mt-4"><strong>Recovery and Heavy Machinery Transport</strong></p>-->
                  
        <!--Text-->
        <div class="d-flex justify-content-center">

            <p class="card-text my-2 lead" style="max-width: 43rem;">we are dedicated to providing excellent transporting and recovery services
                we are always ready to offer you our services. Give us a call or email for more information and we will be happy
                to assist you.
            </p>
        </div>
        <hr class="my-4 rgba-white-light">
        <a  class="btn btn-outline-white font-weight-light font-italic"  href="tel:+355876165971">+355 87 6165971 <i class="fa fa-phone-square ml-1"></i></a>
        <a  class="btn btn-outline-white font-weight-light font-italic" href="mailto:customer-service@brianmcdonnell.ie ">customer-service@brianmcdonnell.ie <i class="fa fa-envelope-open ml-1"></i></a>
        <a  class="btn btn-outline-white font-weight-light font-italic" href="contact">Quick Message <i class="fa fa-envelope ml-1"></i></a>
        <br>
        <a class="nav-link" href="{{ route('admin') }}">admin</a>

    </div>
</section>

<button class="btn btn-outline-dark scroll-top" data-scroll="up" type="button">
    <i class="fa fa-chevron-up"></i>
</button>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script src="{{ URL::asset('js/main.js')}}"></script>
</body>
</html>
