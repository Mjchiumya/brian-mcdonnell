@extends('pages.template_page')

@section('content')
 <div class="mt-5 invisible"></div><br><br>
 <div class="container mt-5">
        <div class="row">
            <div class="col">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Faq</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

        
    <div class="container mb-5">
        <div class="col-md-12">
            <div class="row my-2 border border-secondary p-4">
                <div class="col-md-6">
                    <div class="panel-group" id="accordion9" role="tablist" aria-multiselectable="false">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne9">
                                <h4 class="panel-title">
                                    <a class="" role="button" data-toggle="collapse" data-parent="#accordion9" href="#collapseOne9" aria-expanded="true" aria-controls="collapseOne9">
                                        Do you offer long distance services?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne9">
                                <div class="panel-body">
                                   yes. we do but the best way is to get in touch with us
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo9">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion9" href="#collapseTwo9" aria-expanded="false" aria-controls="collapseTwo9">
                                        Do you do car recovery services?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo9">
                                <div class="panel-body">
                                     yes we do. We do small and heavy goods vehicle recovery
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree9">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion9" href="#collapseThree9" aria-expanded="false" aria-controls="collapseThree9">
                                        is your business open 24/7?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree9">
                                <div class="panel-body">
                                   sometimes certain jobs has to be done on the weekend in those cases we do.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="col-md-6">
                <div class="panel-group" id="accordion9" role="tablist" aria-multiselectable="true">
                    <div class="panel ">
                        <div class="panel-heading" role="tab" id="headingOne9">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion9" href="#collapseFour9" aria-expanded="true" aria-controls="collapseOne9">
                                    Are you limited to services listed on the website?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne9">
                            <div class="panel-body">
                              No. if it is about transportation then talk to us about it by calling or email
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo9">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion9" href="#collapseFive9" aria-expanded="false" aria-controls="collapseTwo9">
                                    Do you do any type of heavy load transport?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo9">
                            <div class="panel-body">
                              yes we do heavy machinery and equipment transportation call us for more information
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default mb-2">
                        <div class="panel-heading" role="tab" id="headingThree9">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion9" href="#collapseSix9" aria-expanded="false" aria-controls="collapseThree9">
                                  Do you do trailer pulling?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSix9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree9">
                            <div class="panel-body">
                              No we don't offer trailer pulling services.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
 @endsection
