@extends('layouts.app')


@section('content')

    <div class="card-body">
      @if (session('status'))
        <div class="alert alert-success" role="alert">
          {{ session('status') }}
         </div>
      @endif
    </div>
@endsection

@section('table')
   <div class="container-fluid">
    <div class="container table-responsive-md" style="background: white;">
          <table class="table table-hover">
            <thead class="thead-light">
            <tr>
                <th>Image</th>
                <th>Tittle</th>
                <th>Comment</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($records as $rec)
            <tr>
                <td class="w-25"><img src={{URL::asset('storage/uploads/'.$rec->image)}} height="200" width="180"/></td>
                <td>{{$rec->name}}</td>
                  <td>{{$rec->description}}</td>

                    <td>
                        <a class="btn btn-danger" href="{{url('/del-job/'.$rec->id)}}"><i class="fa fa-close"></i></a>
                        <a class="btn btn-outline-dark" href="{{url('/img-job/'.$rec->id)}}"><i class="fa fa-upload"></i></a>
                    </td>

            </tr>
           @endforeach
            </tbody>
        </table>
    </div>
    </div>
@endsection
