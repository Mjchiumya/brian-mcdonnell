<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});


Route::get('/home' ,'PagesController@index');
Route::get('/contact' ,'PagesController@contactPage');
Route::get('/our-work' ,'PagesController@workDone');
Route::get('/faq' ,'PagesController@faq');
Route::get('/services' ,'PagesController@services');
Route::get('/login' ,'PagesController@loginPage');
Route::get('/password/reset' ,'PagesController@loginPage');

//add work done items


Auth::routes();

Route::get('/success', 'WorkController@success')->name('admin');
Route::get('/new-job','WorkController@newJob');
Route::get('/img-job/{id}','WorkController@uploadPage');
Route::post('/img-job','WorkController@upload');
Route::get('/del-job/{id}','WorkController@deleteJob');
Route::post('/put-job', 'WorkController@formProcessor');
Route::post('/email','MailController@mailSender');
Route::get('/admin', 'HomeController@index')->name('admin');
