<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Work;
use Illuminate\Support\Facades\Storage;

class WorkController extends Controller
{
    public function formProcessor(Request $request){

        $work = new Work;
        $work->name = $request->input('name');
        $work->description = $request->input('description');

        if ( $work->save()){

            return redirect('success');
        }

        return redirect('admin');

    }

    public function success(){
        return view('success');
    }

    public function newJob(){
        return view('newJob');
    }

    public function uploadPage(){

        return view('upload');
    }

    public function upload(Request $request){

        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'id'   => 'required'
        ]);
        if($request->hasFile('image')){
            $image = $request->file('image');
            //$ext = $image->getClientOriginalExtension();
            $image_name = $image->getClientOriginalName();
            Storage::putFileAs(
                'public/uploads', $image, $image_name
            );

            $job = new Work();
            $job->where('id',$request->post('id'))->update(['image'=>$image_name]);

            return redirect('success');
        }

        return redirect('admin');


        }

    public function deleteJob($id){

    if( Work::destroy($id)){

        return redirect('success');
    }else{
        return redirect('admin');
    }


    }
}
