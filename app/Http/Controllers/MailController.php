<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\inquiry;
class MailController extends Controller
{
    public function mailSender(Request $request)
    {
        $this->validate($request,[
            'name'=> 'required',
            'email'=>'required|email',
            'message'=>'required'
        ]);
        $data = [
            'name'=>$request->post('name'),
            'email'=>$request->post('email'),
            'message'=>$request->post('message')
        ];

        Mail::to('miachaelchiumya@gmail.com')->send(new inquiry($data));
        return back()->with('success', 'Thanks for contacting us!');
    }

}
