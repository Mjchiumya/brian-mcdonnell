<?php

namespace App\Http\Controllers;


use App\Work;

class PagesController extends Controller
{
   public function index(){

       return view('pages.home');
   }

   public function contactPage(){

       return view('pages.contact');
   }

   public function workDone(){

       $work = new Work();
       $jobs = $work::whereNotNull('image')->orderBy('created_at','desc')->get();
       return view('pages.workdone',compact('jobs'));
   }
    public function faq(){

        return view('pages.faq');
    }
    public function services(){

        return view('pages.services');
    }




}
